import java.util.Set; // Importez java.util.Set pour utiliser Set.of()

public class Player {
    public static void main(String[] args) {
        // Afficher le nom complet de l'artiste
        
        Artist artist1 = new Artist("Georges", "Brassens");
        System.out.println("Nom complet de l'artiste : " + artist1.getFullName());
        Artist artist2 = new Artist("Serge", "Gainsbourg");
        Artist artist3 = new Artist("Aya", "Nakamura");

        Music music1 = new Music("Les copains d'abord", 290, Set.of(artist1 , artist2));
        Music music2 = new Music("Je t'aime ... moi non plus", 301, Set.of(artist2));
        Music music3 = new Music("Hype", 321, Set.of(artist3));
        Music music4 = new Music("Bijoux", 321, Set.of(artist3));
        Music music5 = new Music("Parapluie", 321, Set.of(artist1));

        
        Playlist playlist = new Playlist();
        playlist.add(music1);
        playlist.add(music2);
        playlist.add(music3);
        playlist.add(music4);
        playlist.add(music5);
        System.out.println("Total de la durée de la playlist: " + playlist.getTotalDuration() );
        playlist.showPlaylist();
        playlist.remove(music2);
        System.out.println("\n");
        playlist.next();
        playlist.next();
        playlist.next();


       

    //    Playlist playlist2 = new Playlist() ;
    //    System.out.println("playlist test");
    //    playlist2.showPlaylist();
    //    playlist2.next();
    }
   
    
}
