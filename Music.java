import java.util.HashSet;
import java.util.Set;

public class Music {

    private String title;
    private int duration;
    // ensemble (Set) d'objets Artist qui représentent les artistes associés à la
    // musique
    private Set<Artist> artistSet;

    // constructeur de la classe Music
    public Music(String title, int duration, Set<Artist> artistSet) {
        this.title = title;
        this.duration = duration;
        // Crée une nouvelle instance de HashSet à partir du groupe d'artiste passé en
        // paramatre, il garantie qu'il n'y a pas de doublons d'artistes
        this.artistSet = new HashSet<>(artistSet);
    }

    public int getDuration() {
        return this.duration;
    }

    // Méthode de la classe musique qui retourne une chaine de caractere qui
    // contient les infos sur la musique
    public String getInfos() {
        // String result = ("Title : " + title + ", Duration : " +
        // formatDuration(duration) + ", Artist : ")

        // crée un objet StringBuilder, qui est utilisé pour construire des chaînes de
        // caractères
        StringBuilder artistNames = new StringBuilder();

        // boucle for qui itère à travers chaque artiste dans l'ensemble artistSet
        for (Artist artist : artistSet){
            
            // append() méthode utilisée pour ajouter du texte à la fin d'un objet
            // StringBuilder
            // à chaque itération de la boucle, le nom complet de l'artiste est ajouté à
            // l'objet StringBuilder suivi d'une virgule et d'un espace
            artistNames.append(artist.getFullName()).append(", ");
        }
        // supprime les deux derniers caractères (la virgule et l'espace) de la chaîne
        // artistNames
        artistNames.delete(artistNames.length() - 2, artistNames.length());
        // donne le nombre entier de minutes dans la durée totale
        int minutes = this.duration / 60;
        // donne le reste des secondes après avoir converti la durée totale en minutes
        int seconds = this.duration % 60;

        return "Title: " + this.title + ", Duration: " + String.format("%02d:%02d", minutes, seconds) +
                ", Artists: " + artistNames.toString();

    }

}