
### Compilation du projet : 
javac Player.java
### Exécution du projet : 
java Player

### Playlist
- Contient une liste de musique et un ensemble d'artistes 

    Il est possible : 
    
    - d'ajouter une musique à la playlist  
    
    - de supprimer une musique de la playlist
    
    - d'afficher la musique en cours d'éxécution
    
    - d'afficher la prochaine musique 