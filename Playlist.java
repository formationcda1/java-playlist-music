
import java.util.ArrayList;
import java.util.List;
public class Playlist { 

        // Déclare une variable currentMusic de type Music, qui représente la musique actuellement en cours de lecture dans la playlist
    private Music currentMusic; 
    // Déclare une variable musicList de type List (collection) qui contient des objets de type Music, liste des musiques dans la playlist
    private List<Music> musicList; 
    
    //constructeur pour la classe Playlist
    public Playlist() { 
         // crée une nouvelle instance d'ArrayList
        this.musicList = new ArrayList<>();
    }
    
    //méthode add qui permet d'ajouter une musique à la playlist
     public void add(Music music) {
        musicList.add(music);
    }
    //methode remove qui permet de supprimer une musique à la playlist
    public void remove(Music music) {
        musicList.remove(music);
    }
    // méthode getTotalDuration qui calcule la durée totale de toutes les musiques dans la playlist
    public int getTotalDuration() {
        int totalDuration = 0;
        for (Music music : musicList) {
            totalDuration += music.getDuration();
        }
        return totalDuration;
    }
    //methode pour afficher la playlist
    public void showPlaylist(){
        //boucle for pour chaque musique de musicList
        for (var eachMusic : musicList ){
            //affiche les infos
            System.out.println(eachMusic.getInfos());
        }
    }
    //méthode next qui permet de passer à la musique suivante dans la playlist
    public void next() {
        //si la liste de musiques est vide
        if (musicList.isEmpty()) { 
            // affiche un message "playlist vide"
            System.out.println("Playlist vide."); 
            return;
        }  
        // si aucune musique est actuellement en cours de lecture
        if (currentMusic == null) { 
            // la première musique de la playlist sera la musique actuelle
            currentMusic = musicList.get(0); 
            System.out.println("Musique en cours de lecture : " + currentMusic.getInfos());
            return; 
        }
       
        // récupère l'indice de la musique actuelle dans la liste
        int currentIndex = musicList.indexOf(currentMusic); 
        // calcule l'indice de la prochaine musique en prenant en compte le retour au début de la playlist
        int nextIndex = (currentIndex + 1) % musicList.size(); 

        // définit la musique suivante comme musique actuelle
        currentMusic = musicList.get(nextIndex); 
        System.out.println("Prochaine musique " + currentMusic.getInfos());
    }
    
}